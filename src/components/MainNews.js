import React from 'react'
import content from '../images/content.jpg'

const MainNews = () => {
    return (
        <>
            <section className="block-content">
                <div className="block-content__container">
                    <div className="block-content__image">
                        <img src={content} alt="#" />
                    </div>
                    <div className="custom-content__item">
                        <div className="custom-content__label">
                            <span>First Drive</span>
                        </div>
                        <a className="custom-content__title" href="#">
                            <h1>2022 Hyundai Kona N Is Devilishly Fun</h1>
                        </a>
                        <div className="custom-content__dek">
                            <p>
                                Hyundai's rambunctious, 286-hp sport-compact
                                recipe, now in a slightly more practical
                                package.
                            </p>
                            <p>By Derek Powell</p>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default MainNews
