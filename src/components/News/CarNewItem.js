import React from 'react'
import './CarNewItem.css'
import PropTypes from 'prop-types'
import { Button, CardActions } from '@material-ui/core'
import { Link } from 'react-router-dom'
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder'
import FavoriteIcon from '@material-ui/icons/Favorite'
import { connect } from 'react-redux'

const CarNewItem = ({
    id,
    name,
    price,
    image,
    button,
    nameArticle,
    isLiked = false,
    remuveLike,
    addLike,
}) => {
    return (
        <>
            <div className="latest-news__item">
                <a className="latest-news__link">
                    <div className="latest-news__image">
                        <img src={image} alt={name} />
                    </div>
                    <div className="button-news__like">
                        <Button
                            align="right"
                            onClick={() =>
                                isLiked ? remuveLike(id) : addLike(id)
                            }
                        >
                            {isLiked ? (
                                <FavoriteIcon />
                            ) : (
                                <FavoriteBorderIcon />
                            )}
                        </Button>
                    </div>
                    <div className="latest-news__caption">
                        <h3>
                            <Link to={`/product/${nameArticle}`}>{name}</Link>
                        </h3>
                        <p>Starting at ${price} est</p>
                    </div>
                    <CardActions className="car-btn">
                        <Button variant="contained" color="primary">
                            {button}
                        </Button>
                    </CardActions>
                </a>
            </div>
        </>
    )
}

CarNewItem.propTypes = {
    name: PropTypes.string,
    price: PropTypes.number,
    image: PropTypes.string,
}

const mapStateToProps = (state, { id }) => ({
    isLiked: state[id],
})

const mapDispatchToProps = (dispatch) => ({
    addLike: (id) => dispatch({ type: 'LIKE', id }),
    remuveLike: (id) => dispatch({ type: 'DISLIKE', id }),
})

export default connect(mapStateToProps, mapDispatchToProps)(CarNewItem)
