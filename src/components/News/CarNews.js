import React from 'react'
import CarNewItem from './CarNewItem'
import '../News/CarNews.css'
import { Grid } from '@material-ui/core'
// import { useSelector } from 'react-redux'
// import carNewArray from '../News/carNewArray'

const CarNews = ({ FilteredCars }) => {
    // const carNewArray = useSelector((state) => state.product)
    return (
        <>
            <section className="latest-news">
                <div className="latest-news__container">
                    <h2>
                        <span>Car News</span>
                    </h2>
                    <div className="latest-news__grid">
                        {FilteredCars.map(
                            ({
                                id,
                                name,
                                price,
                                image,
                                button,
                                link,
                                nameArticle,
                            }) => (
                                <Grid key={id}>
                                    <CarNewItem
                                        id={id}
                                        name={name}
                                        price={price}
                                        image={image}
                                        button={button}
                                        nameArticle={nameArticle}
                                        link={link}
                                    />
                                </Grid>
                            )
                        )}
                    </div>
                </div>
            </section>
        </>
    )
}

export default CarNews
