import React, { useState } from 'react'
import Header from './Header/Header'
import Nav from './Nav/Nav'
import Main from './Main/Main'
import Footer from './Footer/Footer'
import carNewArray from '../components/News/carNewArray'
import '../../src/pages/articles/Audi/AudiArticles.css'

const App = () => {
    const [FilteredCars, setFilteredCars] = useState(carNewArray)
    const FilterbyCar = (filterAuto) => {
        setFilteredCars(
            carNewArray.filter((item) => item.button.includes(filterAuto))
        )
    }

    return (
        <>
            <Header />
            <Nav FilterbyCar={FilterbyCar} />

            <Main FilteredCars={FilteredCars} />

            <Footer />
        </>
    )
}

export default App
