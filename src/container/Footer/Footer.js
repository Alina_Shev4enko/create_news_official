import React from 'react'
import '../Footer/Footer.css'
import FacebookIcon from '@material-ui/icons/Facebook'
import TwitterIcon from '@material-ui/icons/Twitter'
import { Instagram, Pinterest, Telegram } from '@material-ui/icons'
import { Icon } from '@material-ui/core'

const Footer = () => {
    return (
        <>
            <footer className="footer">
                <div className="footer__container">
                    <div className="footer__logo">
                        <span>Car News Official</span>
                    </div>
                    <div className="footer__social">
                        <a href="#">
                            <FacebookIcon style={{ fontSize: 30 }} />
                        </a>
                        <a href="#">
                            <TwitterIcon style={{ fontSize: 30 }} />
                        </a>
                        <a href="#">
                            <Telegram style={{ fontSize: 30 }} />
                        </a>
                        <a href="#">
                            <Pinterest style={{ fontSize: 30 }} />
                        </a>
                        <a href="#">
                            <Instagram style={{ fontSize: 30 }} />
                        </a>
                    </div>
                    <div className="footer__copy">
                        <p>© 06.08.2021 Alina Shev4enko</p>
                    </div>
                </div>
            </footer>
        </>
    )
}

export default Footer
