import React from 'react'
import background_header from '../../images/background_header.jpg'
import logo from '../../images/logo.png'
import '../Header/Header.css'

const Header = () => {
    return (
        <>
            <div className="header">
                <div className="header-logo">
                    <div className="header-logo__container">
                        <div className="header-logo__image">
                            <img src={background_header} alt="name" />
                        </div>
                        <div className="header-logo__title">
                            <img src={logo} alt="name" />
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Header
