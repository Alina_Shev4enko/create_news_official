import React from 'react'
import { Route } from 'react-router-dom'
import MainNews from '../../components/MainNews'
import CarNews from '../../components/News/CarNews'
import FavoritePage from '../../pages/Favorite/FavoritePage'
import Product from '../../pages/Products/Product'
import '../Main/Main.css'

const Main = ({ FilteredCars }) => {
    return (
        <>
            <Route path="/" exact render={() => <MainNews />} />
            <Route
                path="/"
                exact
                render={() => <CarNews FilteredCars={FilteredCars} />}
            />
            <Route path="/product/:nameArticle" component={Product} />
            <Route path="/favorite" component={FavoritePage} />
        </>
    )
}

export default Main
