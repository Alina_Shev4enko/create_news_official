import React from 'react'
import '../Nav/Nav.css'
import { Link } from 'react-router-dom'

const Nav = ({ FilterbyCar }) => {
    return (
        <>
            <nav className="header-menu">
                <div className="header-menu__container">
                    <div className="header-navbar">
                        <ul className="header-navbar__list">
                            <li className="header-navbar__item">
                                <Link to="/" className="header-navbar__link">
                                    HOME
                                </Link>
                            </li>
                            <li className="header-navbar__item">
                                <a className="header-navbar__link" href="#">
                                    NEW CARS
                                </a>
                            </li>
                            <li className="header-navbar__item drowpdown">
                                <a
                                    className="header-navbar__link drop"
                                    href="#"
                                >
                                    CATEGORY
                                </a>
                                <div className="drowpdown-content">
                                    <a
                                        href="#"
                                        onClick={() => FilterbyCar('Sedan')}
                                    >
                                        SEDAN
                                    </a>
                                    <a
                                        href="#"
                                        onClick={() => FilterbyCar('SUV')}
                                    >
                                        SUV
                                    </a>
                                    <a
                                        href="#"
                                        onClick={() => FilterbyCar('Coupe')}
                                    >
                                        COUPE
                                    </a>
                                </div>
                            </li>
                            <li className="header-navbar__item">
                                <a className="header-navbar__link" href="#">
                                    NEWS
                                </a>
                            </li>
                            <li className="header-navbar__item">
                                <Link
                                    to="/favorite"
                                    className="header-navbar__link"
                                >
                                    FAVORITE
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </>
    )
}

export default Nav
