import React from 'react'
import Audi_RS3 from '../articles/Audi/AudiArticles'
import Cadillac_CT4V from '../articles/Cadillac/ActiclesCadillac'
import Dodge_Challenger from '../articles/Dodge/ArticlesDodge'
import Genesis_G80 from '../articles/Genesis/ArticlesGenesis'
import Hyundai_Santa from '../articles/Hyundai/ArticlesHyundai'
import Land_Rover from '../articles/LandRover/ArticlesLandRover'
import Maserati_MC20 from '../articles/Maserati/ArticlesMaserati'
import Nissan_GTR from '../articles/Nissan/ArticlesNissan'

const Product = ({ match }) => {
    const id = match.params.nameArticle
    const project = (nameArticle) => {
        switch (nameArticle) {
            case 'Audi_RS3':
                return <Audi_RS3 />
            case 'Cadillac_CT4V':
                return <Cadillac_CT4V />
            case 'Dodge_Challenger':
                return <Dodge_Challenger />
            case 'Genesis_G80':
                return <Genesis_G80 />
            case 'Hyundai_Santa':
                return <Hyundai_Santa />
            case 'Land_Rover':
                return <Land_Rover />
            case 'Maserati_MC20':
                return <Maserati_MC20 />
            case 'Nissan_GTR':
                return <Nissan_GTR />
            default:
                return <h1>No project match</h1>
        }
    }
    return <>{project(id)}</>
}

export default Product
