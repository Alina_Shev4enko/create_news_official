import React from 'react'
import audi from '../../../images/articlesCar/audi-rs3_2.jpg'
import audi2 from '../../../images/articlesCar/audi-rs3-sedan.jpg'

const Audi_RS3 = () => {
    return (
        <>
            <section className="content">
                <div className="content__container">
                    <div className="content__inner">
                        <h1>2022 Audi RS3</h1>
                        <p>Starting at $58,000 est SPECS</p>
                    </div>
                    <div className="content__image">
                        <img src={audi2} alt="#" />
                    </div>
                    <div className="content__text">
                        <h2>Overview</h2>
                        <p>
                            With a potent 401-hp turbocharged five-cylinder
                            engine, the 2022 Audi RS3 is the raciest version of
                            the company's small luxury sedan. Based on the
                            four-door Audi A3 and the sportier S3–but blessed
                            with even more power and enhanced chassis tuning–the
                            next-gen RS model will challenge hi-po rivals such
                            as the BMW M2 coupe and Mercedes-AMG CLA45 sedan.
                            The Audi's sharply creased sheetmetal as well as its
                            stylish, nicely appointed interior make it a
                            compelling class player, but we won't know how its
                            performance stacks up until we drive one. We're also
                            still waiting on full details about the 2022 RS3,
                            which is expected to go on sale by the end of the
                            year.
                        </p>

                        <h2>What's New for 2022?</h2>
                        <p>
                            After a hiatus for the 2021 model year, the
                            sportiest variant of Audi's smallest sedan returns
                            for 2022. The RS3 enters its second generation in
                            the U.S. and draws largely from the totally
                            redesigned A3 and S3. While the three share a
                            platform, interior dimensions, and many similar
                            features, the A3 is powered by a 201-hp turbo four
                            and the S3 has a 306-hp version of the same mill.
                            Audi will announce more information and show
                            uncamouflaged images of the new RS3 on July 19.
                        </p>

                        <h2>Pricing and Which One to Buy</h2>
                        <p>
                            Audi hasn't said how much the 2022 RS3 will cost,
                            but we expect it to start just under $60,000. Once
                            we know its official price and learn what equipment
                            is standard and optional, we can recommend the ideal
                            configuration to buy.
                        </p>

                        <h2>Engine, Transmission, and Performance</h2>
                        <p>
                            One of the most distinct elements of the RS3 is its
                            unusual five-cylinder engine. Its odd number of
                            cylinders give it a unique thrumming soundtrack that
                            was particularly pleasing on the outgoing model. The
                            latest iteration is again a turbocharged 2.5-liter,
                            but it now makes 401 horsepower­. Its predecessor
                            also featured a dual-clutch automatic transmission
                            and all-wheel drive. We expect both to be standard
                            on the new RS3, and Audi has said that it'll also
                            have a torque-vectoring rear differential with a
                            drift mode. Likewise, compared with the lesser A3
                            and S3, the sportiest 3 should have upgraded brakes,
                            a stiffer suspension setup, and a unique set of
                            wheels on stickier performance tires.
                        </p>
                        <h2>Interior, Comfort, and Cargo</h2>

                        <div className="content__image">
                            <img src={audi} alt="#" />
                        </div>
                        <p>
                            Inside, the RS3 sedan will look a lot like the A3
                            and S3 variants, with similar passenger and trunk
                            space. Audi endows each with a dashboard design
                            that's anything but bland. A pair of high-mounted
                            air vents face the driver and flank the instrument
                            panel binnacle, which we expect to house a set of
                            digital gauges. Below the center touchscreen are
                            climate controls with physical buttons, and there's
                            a bin below them that's integrated into the center
                            console. Unfortunately, it looks like drivers will
                            have to select gears via a weird, stubby shifter.
                            There's also a circular pad nearby that includes
                            stereo controls, but we'll have to wait and see if
                            its intuitive or annoying.
                        </p>
                    </div>
                </div>
            </section>
        </>
    )
}

export default Audi_RS3
