import React from 'react'
import cadillac from '../../../images/articlesCar/vir-cadillac.jpg'
import cadillac2 from '../../../images/articlesCar/vir-cadillac__2.jpg'

const ArticlesCadillac = () => {
    return (
        <>
            <section className="content">
                <div className="content__container">
                    <div className="content__inner">
                        <h1>2022 Cadillac CT4-V Blackwing</h1>
                        <p>Starting at $59,990</p>
                    </div>
                    <div className="content__image">
                        <img src={cadillac} alt="#" />
                    </div>
                    <div className="content__text">
                        <h2>Overview</h2>
                        <p>
                            With a souped-up engine and track-ready hardware,
                            the 2022 Cadillac CT4-V Blackwing is the hottest
                            version of the brand's smallest sedan. Based on the
                            regular Cadillac CT4, the Blackwing boasts both a
                            standard manual transmission and rear-wheel drive,
                            which serve as twin appetizers for driving
                            enthusiasts. The main course is its spicy 472-hp
                            twin-turbo V-6 and unique chassis tuning, which
                            includes enhanced suspension components and
                            specially developed tires. Cadillac also provides a
                            sense of luxury with a roster of desirable features,
                            but it remains to be seen if the interior quality
                            matches its price tag. Still, the 2022 CT4-V
                            Blackwing looks to capitalize on the company's
                            penchant for producing great-driving cars.
                        </p>

                        <h2>What's New for 2022?</h2>
                        <p>
                            The all-new 2022 CT4-V Blackwing sedan is the
                            revamped successor to the company's once critically
                            acclaimed Cadillac ATS-V. While the Blackwing's
                            official reveal has given us insight into its
                            performance potential, we'll have to wait to get
                            behind the wheel before we can compare it with its
                            predecessor. Still, from what we can tell, the CT4-V
                            Blackwing looks good enough to remind the German
                            manufacturers that America still builds badass
                            performance sedans.
                        </p>

                        <h2>Pricing and Which One to Buy</h2>
                        <p>
                            Starting just shy of $60,000, the 2022 CT4-V
                            Blackwing is priced between cheaper alternatives
                            such as the Mercedes-AMG CLA45 and more expensive
                            ones such as the Alfa Romeo Giulia Quadrifoglio. We
                            like that the Caddy comes standard with a
                            stick-shift transmission and track-ready hardware.
                            The CT4-V Blackwing also offers sportier front seats
                            with some luxury amenities that sound appealing but
                            increase its bottom line. Likewise, Cadillac offers
                            several expensive packages that aim to elevate its
                            sports-sedan status.
                        </p>

                        <h2>Engine, Transmission, and Performance</h2>
                        <p>
                            The CT4-V Blackwing packs a twin-turbo 3.6-liter V-6
                            that produces 472 horsepower and 445 pound-feet of
                            torque. That power is routed to the rear wheels
                            through either a standard six-speed manual
                            transmission or a 10-speed automatic. All-wheel
                            drive is not available. Our first driving
                            impressions revealed the Blackwing sounds better
                            than its ATS-V predecessor, has loads of steering
                            feedback, and possesses one of the best
                            ride-handling balances in its class. Every model is
                            fitted with adaptive dampers, an electronic
                            limited-slip differential, and a powerful braking
                            system that make it racetrack-ready. Although the
                            CT4 rides on the same platform as the discontinued
                            ATS, its performance characteristics have been
                            overhauled. Compared with pedestrian CT4s, the
                            Blackwing has a lower ride height, firmer
                            suspension, bigger wheels and stickier tires, and
                            enhanced aerodynamic addendum that together should
                            make it a formidable sports sedan.
                        </p>
                        <h2>Interior, Comfort, and Cargo</h2>

                        <div className="content__image">
                            <img src={cadillac2} alt="#" />
                        </div>
                        <p>
                            The CT4-V Blackwing's interior has some additional
                            frills compared with the regular CT4 lineup—think
                            carbon-fiber trim, microsuede accents, and more
                            supportive front seats. Otherwise, the design and
                            layout are identical to its toned-down siblings'.
                            Cadillac hasn't exactly exceeded expectations with
                            the quality of Blackwing's cabin, but there's
                            certainly no shortage of fancy features on the
                            roster. The 'Wing comes standard with a 12.0-inch
                            digital gauge cluster and specially bolstered front
                            seats with 18-way power adjustments and heated
                            cushions. These front thrones can be upgraded to a
                            snazzier set with even more aggressive bolstering as
                            well as ventilation and lumbar massage. Since the
                            Blackwing has the same interior layout and cargo
                            space as the regular CT4, it'll have the same useful
                            cubby storage as well as a trunk that held five
                            carry-on suitcases in our testing.
                        </p>
                    </div>
                </div>
            </section>
        </>
    )
}

export default ArticlesCadillac
