import React from 'react'
import dodge from '../../../images/articlesCar/dodge.jpg'
import dodge2 from '../../../images/articlesCar/dodge_2.jpg'

const ArticlesDodge = () => {
    return (
        <>
            <section className="content">
                <div className="content__container">
                    <div className="content__inner">
                        <h1>2021 Dodge Challenger</h1>
                        <p>Starting at $30,945</p>
                    </div>
                    <div className="content__image">
                        <img src={dodge} alt="#" />
                    </div>
                    <div className="content__text">
                        <h2>Overview</h2>
                        <p>
                            History tells us the 2021 Dodge Challenger competes
                            with the Chevy Camaro and Ford Mustang, but reality
                            tells us it has a totally different personality.
                            While all three American muscle cars are headlined
                            by high-powered V-8s, there's a big discrepancy in
                            how they drive. Compared with the Chevy's and the
                            Ford's razor-sharp handling, the Dodge is better
                            suited for relaxed cruising, with a crescendoing V-8
                            exhaust note always ready on command. Those who
                            desire the mightiest version will want the SRT
                            Hellcat (reviewed separately), which makes up to 807
                            horsepower. The Challenger's interior isn't always
                            easy to look at or see out of, but it has comfier
                            seats and more passenger space than its rivals'. Its
                            capacious trunk, segment-exclusive all-wheel-drive
                            option (available on V-6 models only), and roster of
                            colorful paint options and retro-themed appearance
                            packages also make it stand out from the Camaro and
                            Mustang.
                        </p>

                        <h2>What's New for 2022?</h2>
                        <p>
                            The Challenger lineup receives several minor updates
                            for 2021. Dodge now offers a memory feature for
                            models with a power-adjustable steering column,
                            driver's seat, and side mirrors as well as for the
                            radio presets. A set of 20-inch wheels are newly
                            optional on the all-wheel-drive SXT and standard on
                            the all-wheel-drive GT. And R/T Scat Pack buyers can
                            now add SRT branding to their Brembo brake calipers.
                        </p>

                        <h2>Pricing and Which One to Buy</h2>
                        <p>
                            Despite our preference for the V-8-powered Camaro
                            and Mustang, there's a lot of nostalgia and value
                            offered by the Challenger. So we'd maximize both of
                            those attributes, selecting the R/T Scat Pack model.
                            It comes standard with the 485-hp 6.4-liter Hemi V-8
                            (versus the 375-hp 5.7-liter on the regular R/T) and
                            the choice of a six-speed manual or an eight-speed
                            automatic. Believe it or not, we'd opt for the $1595
                            automatic because it's much more responsive than the
                            slushy-feeling stick-shift. We'd also add the
                            adaptive dampers for adjustable ride quality and the
                            Dynamics package for its wide 20-inch wheels,
                            six-piston Brembo front brakes, and leather-wrapped
                            steering wheel. The Plus package improves the
                            interior with ambient lighting, faux-suede seat
                            inserts, and much nicer materials on the dashboard
                            and doors. It requires the Driver Convenience Group,
                            too, which brings blind-spot monitoring,
                            rear-cross-traffic alert, power mirrors, and
                            high-intensity-discharge headlights.
                        </p>

                        <h2>Engine, Transmission, and Performance</h2>
                        <p>
                            The Challenger's base 305-hp V-6 won't satisfy
                            thrill seekers. The modest engine mates exclusively
                            to the eight-speed automatic, but in the heavy
                            Challenger, it lacks the acceleration and excitement
                            of rivals. The Dodge's Hemi V-8 engines are another
                            story. The 375-hp 5.7-liter we tested had plenty of
                            juice to powerslide on demand, and its guttural
                            growl was gratifying. Those looking to maximize the
                            Challenger's potential will want the 6.4-liter V-8,
                            which produces 485 horsepower and 475 pound-feet of
                            torque. We also drove the T/A 392 with the automatic
                            and admired the exhaust's cannon-blast startup sound
                            and baritone roar when prodded. While we're suckers
                            for a manual transmission, the ZF automatic is
                            incredibly responsive to throttle inputs, with quick
                            power-on downshifts. The Challenger hustles through
                            corners like a raging bull seeing red, snorting
                            aggressively and swaying threateningly. The burly
                            Dodge is a muscle car in the truest sense: It's
                            better on the street and the drag strip than on
                            two-lanes and road courses. Since the lineup's
                            redesign in 2015, the models we've driven have
                            offered a compliant ride that's comfortable but a
                            bit unrefined. Compared with the sharper and
                            stickier handling of the Camaro and Mustang,
                            however, the Challenger is too soft in tight turns
                            and its steering is too numb. The slow-to-react helm
                            is well suited to leisurely drives and easily
                            controlled power-induced tail slides.
                        </p>
                        <h2>Interior, Comfort, and Cargo</h2>

                        <div className="content__image">
                            <img src={dodge2} alt="#" />
                        </div>
                        <p>
                            The Challenger has a classic muscle-car interior,
                            with a simple design inspired by its 1970s-era
                            predecessors and comfortable accommodations.
                            Compared with its pony-car rivals, the Dodge is far
                            roomier inside, and adults can actually use the back
                            seat. Unfortunately, its rubberized materials
                            resemble old vinyl rather than premium plastic, and
                            rear visibility is lousy. The Challenger's broad
                            front seats are comfortable for cruising, but even
                            the optional seats, which have added bolstering,
                            don't hug their occupants the way those in the
                            Camaro or Mustang do. Dodge's pony car has an extra
                            seven cubic feet of cargo space in its trunk versus
                            the Camaro. This allows the Challenger to swallow
                            two more bags of luggage than the Camaro. Fold the
                            back seats down and that advantage grows to six. The
                            Challenger has a big center-console bin and a useful
                            spot for a smartphone. Still, none of the cars we
                            tested in this class were particularly adept at
                            storing small items.
                        </p>
                    </div>
                </div>
            </section>
        </>
    )
}

export default ArticlesDodge
