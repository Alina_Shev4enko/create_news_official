import React from 'react'
import genesis from '../../../images/articlesCar/genesis.jpg'
import genesis2 from '../../../images/articlesCar/genesis_2.jpg'

const ArticlesGenesis = () => {
    return (
        <>
            <section className="content">
                <div className="content__container">
                    <div className="content__inner">
                        <h1>2022 Genesis G80</h1>
                        <p>Starting at $49,000</p>
                    </div>
                    <div className="content__image">
                        <img src={genesis} alt="#" />
                    </div>
                    <div className="content__text">
                        <h2>Overview</h2>
                        <p>
                            With a striking exterior design and a posh cabin,
                            the 2022 G80 legitimizes Genesis as a true luxury
                            brand. Two turbocharged engines are offered—a
                            2.5-liter four-cylinder or a 3.5-liter V-6—and both
                            deliver plentiful power and refinement. While the
                            G80 slots in between the smaller G70 and the larger
                            G90 in the Genesis lineup, its interior design is a
                            cut above both of those sedans and it offers a more
                            modern infotainment package too. The G80's fresh
                            take on what makes a premium car premium puts it
                            near the head of a competitive class of luxury
                            sedans which includes established nameplates such as
                            the Audi A6, the BMW 5-series, and the Mercedes-Benz
                            E-class.
                        </p>

                        <h2>What's New for 2022?</h2>
                        <p>
                            A Sport model joins the lineup for 2022 and although
                            we don't have all the information on that version
                            yet, we are expecting it to be offered with both the
                            base turbocharged four-cylinder as well as the
                            turbocharged V-6. Genesis says the Sport will come
                            with a rear-wheel steering system to aid in
                            handling, but we aren't sure yet if any other
                            performance-enhancing options will be offered. Sport
                            models will wear unique design embellishments
                            including slick 20-inch wheels, reworked front and
                            rear bumpers, a three-spoke sport steering wheel,
                            and carbon fiber dashboard trim.
                        </p>

                        <h2>Pricing and Which One to Buy</h2>
                        <p>
                            The best value among the various trim levels appears
                            to be the 2.5T Advanced, which adds several high-end
                            features that easily justify the price increase over
                            the standard car. The Advance trim comes standard
                            with 19-inch wheels, a panoramic sunroof, ventilated
                            front seats, three-zone automatic climate control, a
                            power trunk-lid, and a 21-speaker Lexicon stereo
                            system. All G80s come with rear-wheel drive as
                            standard but all-wheel drive is available for an
                            extra fee.
                        </p>

                        <h2>Engine, Transmission, and Performance</h2>
                        <p>
                            The entry-level G80 comes with a standard
                            turbocharged 2.5-liter inline-four-cylinder engine
                            that makes 300 horsepower. Buyers seeking quicker
                            acceleration will want to opt for the more powerful
                            twin-turbo 3.5-liter V-6 with 375 horsepower. Both
                            engines pair with an eight-speed automatic
                            transmission and either rear- or all-wheel drive.
                            The turbocharged four-cylinder snapped off a perky
                            5.7-second zero-to-60-mph time at our test track
                            while the V-6 made it to 60 mph in 4.9 seconds. The
                            G80 provides a relaxed ride, effortless steering,
                            and dutiful brakes, but it doesn't feel as athletic
                            as some rivals. The Sport model, which is new for
                            2022, is expected to add the athletic element
                            currently missing from the lineup, but we haven't
                            had a chance to sample that car yet.
                        </p>
                        <h2>Interior, Comfort, and Cargo</h2>

                        <div className="content__image">
                            <img src={genesis2} alt="#" />
                        </div>
                        <p>
                            With its primary focus as a luxury car, the G80
                            offers a properly upscale interior. Its cabin
                            showcases an elegant design with a streamlined
                            dashboard, soft-touch surfaces, and rich-looking
                            materials. The steering wheel has an odd aesthetic
                            and the rotary shift knob is never as intuitive as
                            automakers think, especially when positioned near
                            other circular-shaped controls, but overall the
                            inside of the G80 looks wonderful. The sedan offers
                            desirable features such as a 12.3-inch digital gauge
                            cluster, ambient interior lighting, a head-up
                            display, heated and ventilated front and rear seats,
                            and more. The G80 has more headroom and legroom than
                            the outgoing model, too, which was spacious to begin
                            with.
                        </p>
                    </div>
                </div>
            </section>
        </>
    )
}

export default ArticlesGenesis
