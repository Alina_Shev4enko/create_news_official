import React from 'react'
import hyundai from '../../../images/articlesCar/hyundai.jpg'
import hyundai2 from '../../../images/articlesCar/hyundai_2.jpg'

const ArticlesHyundai = () => {
    return (
        <>
            <section className="content">
                <div className="content__container">
                    <div className="content__inner">
                        <h1>2022 Hyundai Santa Cruz</h1>
                        <p>Starting at $25,175</p>
                    </div>
                    <div className="content__image">
                        <img src={hyundai} alt="#" />
                    </div>
                    <div className="content__text">
                        <h2>Overview</h2>
                        <p>
                            Don't get it twisted: the new 2022 Hyundai Santa
                            Cruz is a pickup truck. Its smaller proportions and
                            unibody construction just mean it's no full-size tow
                            rig. Instead, think of the first Hyundai pickup as
                            an alternative to the also-unibody Honda Ridgeline,
                            which too features a one-size-fits-all crew cab,
                            short-bed body style. The Santa Cruz certainly looks
                            more distinct than the Honda–or any other mid-size
                            truck, for that matter­–with its interesting face
                            and creased sheetmetal. Its interior is snazzier
                            than its classmates, too, but the lack of physical
                            controls on some models is disappointing. Its
                            standard four-cylinder engine is also undesirable,
                            but the more powerful turbo option is a pricey
                            proposition. Still, the 2022 Santa Cruz is a more
                            functional twist on the tired crossover formula, and
                            that open box on its butt means it qualifies as a
                            pickup truck.
                        </p>

                        <h2>What's New for 2022?</h2>
                        <p>
                            The idea of a Hyundai pickup truck has been around
                            since the Santa Cruz concept was revealed at the
                            2015 Detroit auto show. That concept has become a
                            reality as the Korean automaker has shown off the
                            production version to the world, and now the all-new
                            model is set to take on undersized trucks such as
                            the forthcoming Ford Maverick and established
                            mid-size pickups.
                        </p>

                        <h2>Pricing and Which One to Buy</h2>
                        <p>
                            The Santa Cruz is available in four trim levels with
                            various features. We think the SEL Premium is the
                            one to get. Unlike the lesser SE and SEL, it has a
                            more powerful engine along with fancier equipment
                            that includes standard all-wheel drive. The system
                            is a $1500 option on the lesser trims. The SEL
                            Premium comes with standard LED headlights,
                            dual-zone climate control, a leather-wrapped
                            steering wheel and shift knob, and an auto-dimming
                            rearview mirror.
                        </p>

                        <h2>Engine, Transmission, and Performance</h2>
                        <p>
                            The Santa Cruz comes with two different powertrain
                            choices. The standard setup is a 2.5-liter
                            four-cylinder that makes 191 horsepower and 181
                            pound-feet of torque. However, based on the languid
                            acceleration it provided the Tuscon crossover we
                            tested, this entry-level engine is best avoided. The
                            upgraded engine is a turbocharged 2.5-liter four
                            with 281 horses and 311 pound-feet. Its added power
                            and punchier responses are much better suited to the
                            Santa Cruz. Both engines mate to eight-speed
                            automatics, but the turbo option partners with the
                            dual-clutch variety. All-wheel drive is also offered
                            with both four-cylinders. The Santa Cruz is shorter
                            and lower than its segment rivals, which helps make
                            it easier to maneuver around town. The
                            top-of-the-line Limited model we drove showcased the
                            truck's refined ride and agile handling.
                        </p>
                        <h2>Interior, Comfort, and Cargo</h2>

                        <div className="content__image">
                            <img src={hyundai2} alt="#" />
                        </div>
                        <p>
                            Inside, the Santa Cruz has one of the nicest cabins
                            among mid-size pickups. Hyundai has made a habit of
                            building vehicles with attractive materials and
                            desirable modern features that would look right in
                            costlier vehicles. It’s the first truck in its class
                            to offer a fully digital gauge cluster, and we
                            appreciate that it has a traditional shifter on the
                            center console instead of a finicky rotary knob or
                            push-button setup. The Santa Cruz is only available
                            with a four-door crew cab, and passenger space in
                            the front and the back is competitive with similarly
                            sized trucks. Its cargo bed is one of the shortest
                            in the segment at about four feet long, but it's
                            very versatile, with a lockable tonneau cover and a
                            useful in-bed trunk that's similar to what the Honda
                            offers.
                        </p>
                    </div>
                </div>
            </section>
        </>
    )
}

export default ArticlesHyundai
