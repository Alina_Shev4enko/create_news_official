import React from 'react'
import land_rover from '../../../images/articlesCar/land_rover.jpg'
import land_rover2 from '../../../images/articlesCar/land_rover_2.jpg'

const ArticlesLandRover = () => {
    return (
        <>
            <section className="content">
                <div className="content__container">
                    <div className="content__inner">
                        <h1>2021 Land Rover Discovery</h1>
                        <p>Starting at $55,250</p>
                    </div>
                    <div className="content__image">
                        <img src={land_rover} alt="#" />
                    </div>
                    <div className="content__text">
                        <h2>Overview</h2>
                        <p>
                            If a Jeep is too conventional—or plain—for your
                            outdoorsy family, consider the 2021 Land Rover
                            Discovery: it provides a much more upscale vibe to
                            accompany its impressive off-road capability. With
                            room for up to seven people inside its posh,
                            three-row cabin, the Discovery adds an extra layer
                            of practicality over the two-row Grand Cherokee. A
                            pair of turbocharged gasoline-powered engines are
                            offered—a four-cylinder and a six-cylinder, both
                            turbocharged—and all Discovery models come with an
                            adjustable air suspension and standard all-wheel
                            drive. The Discovery leverages Land Rover's
                            legendary go-anywhere reputation and backs it up by
                            offering an optional two-speed transfer case and a
                            locking rear differential for maximum overlanding
                            freedom. There's even a Wade mode that's set up to
                            improve the Discovery's water-fording capability.
                            Try that in your Porsche Cayenne or Volvo XC90.
                        </p>

                        <h2>What's New for 2022?</h2>
                        <p>
                            Land Rover has given the Discovery a fairly thorough
                            refresh for 2021 that includes two new turbocharged
                            engines—a 296-hp four-cylinder and a 355-hp
                            six-cylinder—as well as a new Pivi Pro infotainment
                            system with an 11.4-inch display. A new grille,
                            tweaked exterior lighting elements, and new wheel
                            designs give the Disco a visual update, and a new
                            R-Dynamic model—complete with glossy black exterior
                            accents—joins the lineup. Land Rover has improved
                            rear-seat comfort by adding additional padding to
                            the second-row chairs. There's a new 12.3-inch
                            digital gauge display and a full-color head-up
                            display for the driver; a toggle-style gear selector
                            is also new and all Discovery models now come
                            standard with an adjustable air suspension.
                        </p>

                        <h2>Pricing and Which One to Buy</h2>
                        <p>
                            The mid-range R-Dynamic model offers the best value
                            in the Discovery lineup, as it adds fog lamps, LED
                            tail lights, aluminum interior trim, and uniquely
                            styled front and rear bumpers. Base S and R-Dynamic
                            models come standard with the turbo four engine, but
                            the R-Dynamic can be optioned with the more powerful
                            inline-six-cylinder engine for $5550. The top-level
                            HSE trim receives the inline-six as standard
                            equipment.
                        </p>

                        <h2>Engine, Transmission, and Performance</h2>
                        <p>
                            All Discovery models come with full-time all-wheel
                            drive as standard, but buyers can choose between a
                            296-hp turbocharged 2.0-liter four-cylinder or a
                            355-hp turbocharged 3.0-liter inline-six, the latter
                            of which employs a 48-volt hybrid system. We haven't
                            had a chance to test drive the Disco with the base
                            turbo four, but with the inline-six, the big Land
                            Rover feels sprightly around town and delivers
                            adequate power for highway merging and passing. The
                            Land Rover's adventure-ready mission caters to those
                            seeking an SUV for much more than just trolling the
                            parking lot at the mall, but it comes at the expense
                            of on-road dexterity. The Discovery's handling isn't
                            as ponderous as one might expect, but it still feels
                            clumsy and top-heavy when changing direction. The
                            Disco never lets you forget that you're piloting a
                            large SUV. Overall ride quality is quite good and
                            even harsh bumps are dealt with easily. Steering
                            feel, however, is nonexistent, especially on-center,
                            where a large dead spot creates a need for frequent
                            steering corrections when cruising on the highway.
                            If towing a heavy load is on the docket, the Disco
                            is rated to tow up to 8200 pounds.
                        </p>
                        <h2>Interior, Comfort, and Cargo</h2>

                        <div className="content__image">
                            <img src={land_rover2} alt="#" />
                        </div>
                        <p>
                            The Discovery's interior materials are upscale and
                            the design is appropriately posh. Luxury features
                            abound throughout the cabin, and the level of
                            comfort only increases as you climb through the more
                            expensive trims. Power-adjustable, heated leather
                            seats with optional massage function for the driver
                            and front-seat passenger offer good support and even
                            better cushioning; second-row seats provide
                            fore-and-aft and recline adjustments. Without
                            adequate space to haul gear there would be fewer
                            opportunities for adventure. With both of its rear
                            rows of seats folded, the Disco's spacious cabin
                            proved to be quite accommodating with room for 29 of
                            our carry-on suitcases. But flip the last row of
                            seats up and load the Discovery with the full
                            complement of seven would-be explorers and the
                            maximum number of carry-ons it can carry drops to
                            just two behind the third row.
                        </p>
                    </div>
                </div>
            </section>
        </>
    )
}

export default ArticlesLandRover
