import React from 'react'
import maserati from '../../../images/articlesCar/maserati.jpg'
import maserati2 from '../../../images/articlesCar/maserati_2.jpg'

const ArticlesMaserati = () => {
    return (
        <>
            <section className="content">
                <div className="content__container">
                    <div className="content__inner">
                        <h1>2022 Maserati MC20</h1>
                        <p>Starting at $213,000 est</p>
                    </div>
                    <div className="content__image">
                        <img src={maserati} alt="#" />
                    </div>
                    <div className="content__text">
                        <h2>Overview</h2>
                        <p>
                            Maserati has been teasing us with the new MC20
                            mid-engine sports car for some time now, and we've
                            finally driven this sweet little cannoli and sampled
                            its novel 621-hp twin-turbo V-6. The MC20 is offered
                            as a two-seater coupe with a minimalist cabin that's
                            focused on the driver; a convertible is expected to
                            join the lineup later and eventually an electric
                            MC20 will reach production too. A collaboration with
                            motorsport engineering company Dallara has resulted
                            in a carbon-fiber structure with aluminum subframes
                            for the suspension and powertrain. This construction
                            minimizes weight, which sets the MC20 up for
                            performance-related success.
                        </p>

                        <h2>What's New for 2022?</h2>
                        <p>
                            The MC20 is an all-new model for the Maserati
                            lineup, and the "MC" in its name refers to Maserati
                            Corse—a signal that the brand will soon re-enter
                            racing events with a track-only variant of the car.
                        </p>

                        <h2>Pricing and Which One to Buy</h2>
                        <p>
                            Maserati hasn't launched the convertible body style
                            yet, but it will almost certainly command a hefty
                            premium over the coupe. We'd save the money and go
                            with the coupe anyway. Most of the MC20's most
                            desirable features come standard, but we'd suggest
                            springing for the optional front suspension lifter
                            to help it clear any speed bumps.
                        </p>

                        <h2>Engine, Transmission, and Performance</h2>
                        <p>
                            Maserati is using the MC20 as an opportunity to
                            launch an all-new twin-turbocharged V-6 engine that
                            pumps out 630 hp. Called Nettuno, the new engine
                            utilizes a unique twin-combustion system borrowed
                            from Formula 1 race cars and is paired to an
                            eight-speed automatic transmission. Maserati claims
                            a zero-to-62-mph time of less than three seconds and
                            a top speed of over 200 mph. In addition to the
                            gasoline-powered model, Maserati also says an
                            all-electric variant of the MC20 with all-wheel
                            drive will hit the market in short order and will be
                            the Italian luxury brand's first foray into
                            electrification. The company hasn't released any
                            details about the battery or electric motors yet,
                            but we expect to hear more about the EV model soon.
                            After our brief test drive of the gasoline-powered
                            MC20, we came away impressed with the powertrain's
                            throttle response and linear power delivery. It's As
                            expected, the MC20 is blisteringly quick and
                            handling is race-car sharp. Despite the MC20's
                            otherworldly performance, its ride is refined and
                            with the adaptive suspension in its most comfortable
                            mode it soaks up bumps well enough to consider
                            driving it daily or taking on a road trip.
                        </p>
                        <h2>Interior, Comfort, and Cargo</h2>

                        <div className="content__image">
                            <img src={maserati2} alt="#" />
                        </div>
                        <p>
                            A two-seat cabin is accessed through upward-opening
                            'butterfly' doors. The MC20's interior design is
                            clean and minimalist, with only the essential
                            controls dotting the center console. The cabin
                            offers upscale materials with a decidedly sporty
                            bent, including black leather seats with dark blue
                            contrast stitching, aluminum pedals, and carbon
                            fiber trim. To help solve the issue of rearward
                            visibility, the MC20 features a digital rear-view
                            mirror that uses a rear-facing camera to feed an
                            image of what's behind the car to a frameless inside
                            rear view mirror mounted to the windshield in the
                            traditional location. Several current-production
                            vehicles already offer a similar system.
                        </p>
                    </div>
                </div>
            </section>
        </>
    )
}

export default ArticlesMaserati
