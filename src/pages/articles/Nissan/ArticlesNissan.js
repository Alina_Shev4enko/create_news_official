import { Android } from '@material-ui/icons'
import React from 'react'
import nissan from '../../../images/articlesCar/nissan.jpg'
import nissan2 from '../../../images/articlesCar/nissan_2.jpg'

const ArticlesNissan = () => {
    return (
        <>
            <section className="content">
                <div className="content__container">
                    <div className="content__inner">
                        <h1>2022 Nissan GT-R</h1>
                        <p>Starting at $115,000 est</p>
                    </div>
                    <div className="content__image">
                        <img src={nissan} alt="#" />
                    </div>
                    <div className="content__text">
                        <h2>Overview</h2>
                        <p>
                            With up to 600-hp under its hood, the 2022 Nissan
                            GT-R is a powerful and tenacious-handling sports car
                            that earns its nickname of Godzilla. Unfortunately,
                            its formula hasn't changed much since it was
                            introduced in 2009 but the sports cars it competes
                            with have. Inside is where the GT-R's age becomes
                            more apparent, with an outdated design and
                            down-market materials that are shared with lesser
                            Nissan models. The GT-R does have several things
                            going for it though. For one thing, all-wheel drive
                            is standard on every model which helps with handling
                            and gives the car a planted, confident feel. Plus,
                            it's more rare than a Porsche 911, so you'll stand
                            out in traffic or at your next track day.
                        </p>

                        <h2>What's New for 2022?</h2>
                        <p>
                            Nissan is releasing a Special Edition model of the
                            GT-R based on the 600-hp NISMO trim for 2022. The
                            car will be sold only in limited numbers, although
                            Nissan has not yet said how many will come to North
                            America. Special Edition models will be identified
                            by unique black-painted 20-inch wheels with red
                            accents, a carbon-fiber hood, and a special Stealth
                            Gray exterior paint color.
                        </p>

                        <h2>Pricing and Which One to Buy</h2>
                        <p>
                            Somehow Nissan charges six figures for a car that
                            cost less than that when it debuted a decade ago—and
                            hasn't changed much since. Still, those seeking
                            permanent seat time in the mightiest car to carry
                            the GT-R badge aren't worried about price. While the
                            ultimate version is the NISMO, it costs more than
                            the exotic McLaren 570 and Porsche 911 Turbo S. In
                            light of this, we'd suggest the least-expensive GT-R
                            Premium.
                        </p>

                        <h2>Engine, Transmission, and Performance</h2>
                        <p>
                            The 2022 GT-R's standard twin-turbo 3.8-liter V-6
                            makes a mighty 565 horsepower. It hooks up to a
                            six-speed automatic transmission and all-wheel drive
                            that conspire to put all that power to the pavement.
                            At our test track, the GT-R launched itself from
                            zero to 60 mph in a mere 2.9 seconds. The GT-R's
                            quick steering, rigid structure, and adjustable
                            suspension can make even amateurs feel positively
                            heroic from behind the wheel. Want more? Check out
                            the Track Edition and NISMO models with a tuned-up
                            engine that makes 600 horsepower. The ride is firm
                            but not punishing and, thanks to active sound
                            cancellation, the thrum of the GT-R's engine doesn't
                            punish your ear drums when cruising on the highway.
                        </p>
                        <h2>Interior, Comfort, and Cargo</h2>

                        <div className="content__image">
                            <img src={nissan2} alt="#" />
                        </div>
                        <p>
                            Inside, the GT-R's front seats are plenty roomy but
                            the rear seats are places only small children could
                            find comfortable. The interior is nicely appointed
                            and offers a host of standard features, but those
                            seeking a high-end interior such as those of the
                            Audi R8 or the Mercedes-AMG GT will be disappointed.
                            Every model features a dual-zone climate control,
                            leather-and-suede-covered upholstery, heated front
                            seats, and more. Interior cubby storage is scarce
                            with nothing more than large door pockets and a
                            small center-console bin.
                        </p>
                    </div>
                </div>
            </section>
        </>
    )
}

export default ArticlesNissan
